# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="xxd, ripped from vim-core"
HOMEPAGE="https://vim.sourceforge.io/ https://github.com/vim/vim"
SRC_URI=""

LICENSE="vim"
SLOT="0"
KEYWORDS="alpha amd64 ~arm ~arm64 hppa ia64 ~m68k ~mips ppc ppc64 ~riscv s390 ~sh sparc x86 ~ppc-aix ~x64-cygwin ~amd64-fbsd ~x86-fbsd ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~m68k-mint ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"

RDEPEND="!app-editors/vim-core"

src_unpack() {
	mkdir -p "${P}"
	cp "${FILESDIR}/Makefile" "${P}/Makefile"
	cp "${FILESDIR}/xxd.c" "${P}/xxd.c"
}

src_install() {
	dobin xxd
}
