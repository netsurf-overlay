# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

NETSURF_BUILDSYSTEM="buildsystem-9999"
NETSURF_BUILDSYSTEM_SRC_URI="git://git.netsurf-browser.org/buildsystem.git -> netsurf-${NETSURF_BUILDSYSTEM}"

inherit netsurf git-r3

DESCRIPTION="HTML5 compliant parsing library, written in C"
HOMEPAGE="http://www.netsurf-browser.org/projects/hubbub/"
EGIT_REPO_URI="git://git.netsurf-browser.org/libhubbub.git"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~m68k-mint"
IUSE="doc test"

RDEPEND=">=dev-libs/libparserutils-9999[static-libs?,${MULTILIB_USEDEP}]
	!net-libs/hubbub"
DEPEND="${RDEPEND}
	test? ( dev-lang/perl
		>=dev-libs/json-c-0.10-r1[${MULTILIB_USEDEP}] )
	>=dev-util/netsurf-buildsystem-9999"

DOCS=( README docs/{Architecture,Macros,Todo,Treebuilder,Updated} )

RESTRICT=test

src_unpack() {
	git-r3_src_unpack

	cp -r /usr/share/netsurf-buildsystem "${WORKDIR}/${NETSURF_BUILDSYSTEM}"
}
