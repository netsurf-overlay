# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-libs/libparserutils/libparserutils-0.2.0.ebuild,v 1.1 2014/11/06 18:07:14 xmw Exp $

EAPI=5

NETSURF_BUILDSYSTEM="buildsystem-9999"

inherit flag-o-matic netsurf git-r3

DESCRIPTION="NetSurf generalised utility library"
HOMEPAGE="http://source.netsurf-browser.org/libnsutils.git/"
EGIT_REPO_URI="http://git.netsurf-browser.org/libnsutils.git"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=">=dev-util/netsurf-buildsystem-9999"

DOCS=( )

PATCHES=( "${FILESDIR}/${P}-CFLAGS.patch"
		"${FILESDIR}/${P}-libnsutils.pc.in.patch"
)

src_unpack() {
	git-r3_src_unpack

	cp -r /usr/share/netsurf-buildsystem "${WORKDIR}/${NETSURF_BUILDSYSTEM}"
}

src_configure() {
	netsurf_src_configure
}
