# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

NETSURF_BUILDSYSTEM="buildsystem-9999"

inherit flag-o-matic netsurf git-r3

DESCRIPTION="A library for processing UTF-8 encoded Unicode strings"
HOMEPAGE="http://www.public-software-group.org/utf8proc"
EGIT_REPO_URI="http://git.netsurf-browser.org/libutf8proc.git"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=">=dev-util/netsurf-buildsystem-9999"

DOCS=( README )

PATCHES=( "${FILESDIR}"/${P}-CFLAGS.patch )

src_unpack() {
	git-r3_src_unpack

	cp -r /usr/share/netsurf-buildsystem "${WORKDIR}/${NETSURF_BUILDSYSTEM}"
}
