# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

NETSURF_BUILDSYSTEM="buildsystem-9999"

NETSURF_COMPONENT_TYPE=binary
inherit netsurf git-r3

DESCRIPTION="generate javascript to dom bindings from w3c webidl files"
HOMEPAGE="http://www.netsurf-browser.org/"
EGIT_REPO_URI="http://git.netsurf-browser.org/nsgenbind.git"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~m68k-mint"
IUSE=""

DEPEND="virtual/yacc
	>=dev-util/netsurf-buildsystem-9999"

src_unpack() {
	git-r3_src_unpack

	cp -r /usr/share/netsurf-buildsystem "${WORKDIR}/${NETSURF_BUILDSYSTEM}"
}
