# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-libs/libcss/libcss-0.5.0-r1.ebuild,v 1.1 2015/06/09 22:02:57 xmw Exp $

EAPI=5

NETSURF_BUILDSYSTEM="buildsystem-9999"

inherit netsurf git-r3

DESCRIPTION="CSS parser and selection engine, written in C"
HOMEPAGE="http://www.netsurf-browser.org/projects/libcss/"
EGIT_REPO_URI="http://git.netsurf-browser.org/libcss.git"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~m68k-mint"
IUSE="test"

RDEPEND=">=dev-libs/libparserutils-9999[static-libs?,${MULTILIB_USEDEP}]
	>=dev-libs/libwapcaplet-9999[static-libs?,${MULTILIB_USEDEP}]"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	test? ( dev-lang/perl )
	>=dev-util/netsurf-buildsystem-9999"

PATCHES=( "${FILESDIR}/${P}-glibc2.20.patch"
		  "${FILESDIR}/${P}-add-mona-family.patch"
		)

src_unpack() {
	git-r3_src_unpack

	cp -r /usr/share/netsurf-buildsystem "${WORKDIR}/${NETSURF_BUILDSYSTEM}"
}
