# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5
NETSURF_COMPONENT_TYPE=binary
NETSURF_BUILDSYSTEM="buildsystem-9999"

inherit netsurf git-r3

DESCRIPTION="a free, open source web browser"
HOMEPAGE="http://www.netsurf-browser.org/"
SRC_URI="http://xmw.de/mirror/netsurf-fb.modes-example.gz"
EGIT_REPO_URI="http://git.netsurf-browser.org/netsurf.git"

LICENSE="GPL-2 MIT"
SLOT="0"
KEYWORDS=""
IUSE="+bmp fbcon truetype +gif gstreamer gtk2 gtk3 javascript +jpeg +mng +png
	+rosprite +svg +svgtiny fbcon_frontend_able fbcon_frontend_linux
	fbcon_frontend_sdl fbcon_frontend_vnc fbcon_frontend_x"

REQUIRED_USE="
	amd64? ( abi_x86_32? (
		!gstreamer !javascript svg? ( svgtiny ) !truetype ) )
	fbcon? ( ^^ ( fbcon_frontend_able fbcon_frontend_linux fbcon_frontend_sdl
		fbcon_frontend_vnc fbcon_frontend_x ) )"

RDEPEND="dev-libs/libxml2
	net-misc/curl
	>=dev-libs/libcss-9999[${MULTILIB_USEDEP}]
	>=net-libs/libhubbub-9999[${MULTILIB_USEDEP}]
	>=net-libs/libdom-9999[xml,${MULTILIB_USEDEP}]
	>=dev-libs/libutf8proc-9999
	>=dev-libs/libnsutils-9999
	bmp? ( >=media-libs/libnsbmp-9999[${MULTILIB_USEDEP}] )
	fbcon? ( >=dev-libs/libnsfb-9999[${MULTILIB_USEDEP}]
		truetype? ( media-fonts/dejavu
			>=media-libs/freetype-2.5.0.1[${MULTILIB_USEDEP}] )
	)
	gif? ( >=media-libs/libnsgif-9999[${MULTILIB_USEDEP}] )
	gtk2? ( >=dev-libs/glib-2.34.3:2[${MULTILIB_USEDEP}]
		gnome-base/libglade:2.0
		>=x11-libs/gtk+-2.24.23:2[${MULTILIB_USEDEP}] )
	gtk3? ( >=dev-libs/glib-2.34.3:2[${MULTILIB_USEDEP}]
		gnome-base/libglade:2.0
		>=x11-libs/gtk+-3.00.00:3[${MULTILIB_USEDEP}] )
	gstreamer? ( media-libs/gstreamer:0.10 )
	javascript? ( >=dev-libs/nsgenbind-9999 )
	jpeg? ( >=virtual/jpeg-0-r2[${MULTILIB_USEDEP}] )
	mng? ( >=media-libs/libmng-1.0.10-r2[${MULTILIB_USEDEP}] )
	png? ( >=media-libs/libpng-1.2.51[${MULTILIB_USEDEP}] )
	svg? ( svgtiny? ( >=media-libs/libsvgtiny-9999[${MULTILIB_USEDEP}] )
		!svgtiny? ( gnome-base/librsvg:2 ) )
	( || ( app-editors/vim-core app-editors/xxd ) )"
DEPEND="${RDEPEND}
	javascript? ( >=dev-libs/nsgenbind-0.1.1 )
	rosprite? ( >=media-libs/librosprite-0.1.1[${MULTILIB_USEDEP}] )
	>=dev-util/netsurf-buildsystem-9999"

PATCHES=(
	"${FILESDIR}"/${P}-reenable-glib-deprecated.patch
)
DOCS=( fb.modes README.md docs/using-framebuffer.md
	docs/ideas/{cache,css-engine,render-library}.txt )

src_unpack() {
	git-r3_src_unpack
	unpack "netsurf-fb.modes-example.gz"
	cp -r /usr/share/netsurf-buildsystem "${WORKDIR}/${NETSURF_BUILDSYSTEM}"
}

src_prepare() {
	rm -rf amiga atari beos cocoa monkey riscos windows  || die

	mv "${WORKDIR}"/netsurf-fb.modes-example fb.modes

	netsurf_src_prepare
}

src_configure() {
	netsurf_src_configure

	netsurf_makeconf+=(
		NETSURF_USE_BMP=$(usex bmp YES NO)
		NETSURF_USE_GIF=$(usex gif YES NO)
		NETSURF_USE_JPEG=$(usex jpeg YES NO)
		NETSURF_USE_PNG=$(usex png YES NO)
		NETSURF_USE_PNG=$(usex png YES NO)
		NETSURF_USE_MNG=$(usex mng YES NO)
		NETSURF_USE_VIDEO=$(usex gstreamer YES NO)
		NETSURF_USE_JS=NO
		NETSURF_USE_MOZJS=NO
		NETSURF_USE_DUKTAPE=$(usex javascript YES NO)
		NETSURF_USE_HARU_PDF=NO
		NETSURF_USE_NSSVG=$(usex svg $(usex svgtiny YES NO) NO)
		NETSURF_USE_RSVG=$(usex svg $(usex svgtiny NO YES) NO)
		NETSURF_USE_ROSPRITE=$(usex rosprite YES NO)
		PKG_CONFIG=$(tc-getPKG_CONFIG)
		$(usex fbcon_frontend_able  NETSURF_FB_FRONTEND=able  "")
		$(usex fbcon_frontend_linux NETSURF_FB_FRONTEND=linux "")
		$(usex fbcon_frontend_sdl   NETSURF_FB_FRONTEND=sdl   "")
		$(usex fbcon_frontend_vnc   NETSURF_FB_FRONTEND=vnc   "")
		$(usex fbcon_frontend_x     NETSURF_FB_FRONTEND=x     "")
		NETSURF_FB_FONTLIB=$(usex truetype freetype internal)
		NETSURF_FB_FONTPATH=${EROOT}usr/share/fonts/dejavu
		TARGET=dummy
	)
}

src_compile() {
	if use fbcon ; then
		netsurf_makeconf=( "${netsurf_makeconf[@]/TARGET=*/TARGET=framebuffer}" )
		netsurf_src_compile
	fi
	if use gtk2 ; then
		netsurf_makeconf=( "${netsurf_makeconf[@]/TARGET=*/TARGET=gtk2}" )
		netsurf_src_compile
	fi
	if use gtk3 ; then
		netsurf_makeconf=( "${netsurf_makeconf[@]/TARGET=*/TARGET=gtk3}" )
		netsurf_src_compile
	fi
}

src_install() {
	sed -e '1iexit;' \
		-i "${WORKDIR}"/*/utils/git-testament.pl || die

	if use fbcon ; then
		netsurf_makeconf=( "${netsurf_makeconf[@]/TARGET=*/TARGET=framebuffer}" )
		netsurf_src_install
		elog "framebuffer binary has been installed as netsurf-fb"
		make_desktop_entry "${EROOT}"usr/bin/netsurf-fb NetSurf-framebuffer netsurf "Network;WebBrowser"

		elog "In order to setup the framebuffer console, netsurf needs an /etc/fb.modes"
		elog "You can use an example from /usr/share/doc/${PF}/fb.modes.* (bug 427092)."
		elog "Please make /etc/input/mice readable to the account using netsurf-fb."
		elog "Either use chmod a+r /etc/input/mice (security!!!) or use an group."
	fi
	if use gtk2 ; then
		netsurf_makeconf=( "${netsurf_makeconf[@]/TARGET=*/TARGET=gtk2}" )
		netsurf_src_install
	fi
	if use gtk3 ; then
		netsurf_makeconf=( "${netsurf_makeconf[@]/TARGET=*/TARGET=gtk3}" )
		netsurf_src_install
	fi

	insinto /usr/share/pixmaps
	doins frontends/gtk/res/netsurf.xpm
}
