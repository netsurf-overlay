# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

NETSURF_BUILDSYSTEM="buildsystem-9999"

inherit git-r3

DESCRIPTION="Netsurf buildsystem"
HOMEPAGE="http://www.netsurf-browser.org/"
EGIT_REPO_URI="http://git.netsurf-browser.org/buildsystem.git"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=""

DOCS=( README )

src_compile() {
	:
}

src_install() {
	dodir /usr/share/netsurf-buildsystem/
	cp -R "${S}/citools" "${D}/usr/share/netsurf-buildsystem/"
	cp -R "${S}/example" "${D}/usr/share/netsurf-buildsystem/"
	cp -R "${S}/llvm" "${D}/usr/share/netsurf-buildsystem/"
	cp -R "${S}/Makefile" "${D}/usr/share/netsurf-buildsystem/"
	cp -R "${S}/makefiles" "${D}/usr/share/netsurf-buildsystem/"
	cp -R "${S}/testtools" "${D}/usr/share/netsurf-buildsystem/"
}
