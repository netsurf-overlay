# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

NETSURF_BUILDSYSTEM="buildsystem-9999"

inherit netsurf git-r3

DESCRIPTION="decoding library for the GIF image file format, written in C"
HOMEPAGE="http://www.netsurf-browser.org/projects/libnsgif/"
EGIT_REPO_URI="http://git.netsurf-browser.org/libnsgif.git"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~m68k-mint"
IUSE=""

RDEPEND=""
DEPEND="virtual/pkgconfig
	>=dev-util/netsurf-buildsystem-9999"

PATCHES=( "${FILESDIR}"/${P}-glibc2.20.patch )

src_unpack() {
	git-r3_src_unpack

	cp -r /usr/share/netsurf-buildsystem "${WORKDIR}/${NETSURF_BUILDSYSTEM}"
}
