# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

NETSURF_BUILDSYSTEM="buildsystem-9999"

inherit netsurf git-r3

DESCRIPTION="framebuffer abstraction library, written in C"
HOMEPAGE="http://www.netsurf-browser.org/projects/libsvgtiny/"
EGIT_REPO_URI="http://git.netsurf-browser.org/libsvgtiny.git"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~m68k-mint"
IUSE=""

RDEPEND=">=net-libs/libdom-9999[xml,static-libs?,${MULTILIB_USEDEP}]
	>=dev-libs/libwapcaplet-9999[static-libs?,${MULTILIB_USEDEP}]"
DEPEND="${RDEPEND}
	dev-util/gperf
	virtual/pkgconfig
	>=dev-util/netsurf-buildsystem-9999"

PATCHES=( "${FILESDIR}"/${P}-glibc2.20.patch
	"${FILESDIR}"/${P}-parallel-build.patch )

src_unpack() {
	git-r3_src_unpack

	cp -r /usr/share/netsurf-buildsystem "${WORKDIR}/${NETSURF_BUILDSYSTEM}"
}
