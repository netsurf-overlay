# Copyright 1999-2020 Gentoo Foundation
# Copyright 1999-2019 Gentoo Authors

EAPI=5

NETSURF_BUILDSYSTEM="buildsystem-9999"

inherit netsurf git-r3

DESCRIPTION="framebuffer abstraction library, written in C"
HOMEPAGE="http://www.netsurf-browser.org/projects/librosprite/"
EGIT_REPO_URI="http://git.netsurf-browser.org/librosprite.git"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~m68k-mint"
IUSE=""

RDEPEND=""
DEPEND="virtual/pkgconfig
	>=dev-util/netsurf-buildsystem-9999"

src_unpack() {
	git-r3_src_unpack

	cp -r /usr/share/netsurf-buildsystem "${WORKDIR}/${NETSURF_BUILDSYSTEM}"
}

